{
	"plugins": ["phsignal", "xdotool", "irw", "osd", "launcher", "cinnamon", "purple", "phi", "keybind", "timer"],
	"options": {
		"jobs": 2,
		"plugins": {
			"phsignal": {"signals": ["interrupt", "quit", "terminate"]},
			"keybind": {"keys": ["Super-Q", "Ctrl-K"]}
		}
	},
	"events": {
		"signal.interrupt": ["exit"],
		
		"irw.KEY_SLEEP": ["close_window"],
		"irw.KEY_TV": ["alrm"],
		"irw.KEY_AUDIO": ["music"],
		"irw.KEY_CAMERA": ["pictures"],
		"irw.KEY_VIDEO": ["video"],
		
		"irw.KEY_STOP": ["stop"],
		"irw.KEY_RECORD": ["space"],
		"irw.KEY_PLAY": ["play"],
		"irw.KEY_PAUSE": ["pause"],
		"irw.KEY_REWIND": ["left_arrow"],
		"irw.KEY_FASTFORWARD": ["right_arrow"],
		"irw.KEY_PREVIOUS": ["s_key"],
		"irw.KEY_NEXT": ["s_key"],
		
		"irw.KEY_EXIT": ["backspace"],
		"irw.KEY_UP": ["mouse_up"],
		"irw.KEY_DOWN": ["mouse_down"],
		"irw.KEY_LEFT": ["mouse_left"],
		"irw.KEY_RIGHT": ["mouse_right"],
		"irw.KEY_OK": ["mouse_left_click"],
		"irw.KEY_INFO": ["mouse_right_click"],
		
		"irw.KEY_VOLUMEUP": ["volume_up"],
		"irw.KEY_VOLUMEDOWN": ["volume_down"],
		"irw.KEY_MUTE": ["mute"],
		"irw.KEY_MEDIA": ["kodi"],
		"irw.KEY_CHANNELUP": ["up_arrow"],
		"irw.KEY_CHANNELDOWN": ["down_arrow"],
		
		"irw.KEY_PVR": ["space"],
		"irw.KEY_EPG": ["space"],
		"irw.KEY_TUNER": ["ev", "timed"],
		"irw.KEY_DVD": ["cond","timed"],
		
		"irw.KEY_NUMERIC_1": ["one"],
		"irw.KEY_NUMERIC_2": ["two"],
		"irw.KEY_NUMERIC_3": ["three"],
		"irw.KEY_NUMERIC_4": ["four"],
		"irw.KEY_NUMERIC_5": ["five"],
		"irw.KEY_NUMERIC_6": ["six"],
		"irw.KEY_NUMERIC_7": ["seven"],
		"irw.KEY_NUMERIC_8": ["eight"],
		"irw.KEY_NUMERIC_9": ["nine"],
		"irw.KEY_NUMERIC_0": ["zero"],
		"irw.KEY_NUMERIC_STAR": ["star"],
		"irw.KEY_NUMERIC_POUND": ["pound"],
		"irw.KEY_DELETE": ["backspace"],
		"irw.KEY_ENTER": ["enter"],
		"cinnamon.lock": ["away"],
		"cinnamon.unlock": ["available"]
	},
	"macros": {
		"exit": [{"action": "phantom.exit"}],
		"alrm": [{"action": "signal.alarm", "config":{"time": 60}}],
		
		"close_window": [{"action": "xdotool.keyboard.key", "config": {"key": "alt+F4"}}],
		"music": [{"action": "xdotool.keyboard.key", "config": {"key": "XF86Music"}}],
		"video": [{"action": "xdotool.keyboard.key", "config": {"key": "XF86Video"}}],
		"pictures": [{"action": "xdotool.keyboard.key", "config": {"key": "XF86Pictures"}}],
		"space": [
			{"action": "xdotool.keyboard.key", "config": {"key": "space"}},
			{"action": "phi.set", "config": {"key": "return", "value": "boop"}},
			{"action": "osd.display.text", "config": {"text": "hello"}},
			{"action": "phi.get", "config": {"key": "return"}},
			{"action": "osd.display.text", "config": {"text": "$prev"}},
			{"action": "phi.unset", "config": {"key": "return"}}
		],
		"cond": [
			{"action": "phi.if", "config": {"if": "1",
				"true": "irw.KEY_PVR",
				"false": "irw.KEY_EPG"
			}}
		],
		"timed": [
			{"action": "timer.set", "config": {"name": "example", "duration": 5}}
		],
		"ev": [{"action": "phi.event", "config": {"event": "irw.KEY_PVR"}}],
		"play": [{"action": "xdotool.keyboard.key", "config": {"key": "XF86AudioPlay"}}],
		"pause": [{"action": "xdotool.keyboard.key", "config": {"key": "XF86AudioPause"}}],
		"stop": [{"action": "xdotool.keyboard.key", "config": {"key": "XF86AudioStop"}}],
		"left_arrow": [{"action": "xdotool.keyboard.key", "config": {"key": "Left"}}],
		"right_arrow": [{"action": "xdotool.keyboard.key", "config": {"key": "Right"}}],
		"s_key": [{"action": "xdotool.keyboard.key", "config": {"key": "s"}}],
		
		"backspace": [{"action": "xdotool.keyboard.key", "config": {"key": "BackSpace"}}],
		"mouse_up": [{"action": "xdotool.mouse.move", "config": {"x": "0", "y": "-10"}}],
		"mouse_down": [{"action": "xdotool.mouse.move", "config": {"x": "0", "y": "10"}}],
		"mouse_left": [{"action": "xdotool.mouse.move", "config": {"x": "-10", "y": "0"}}],
		"mouse_right": [{"action": "xdotool.mouse.move", "config": {"x": "10", "y": "0"}}],
		"mouse_left_click": [{"action": "xdotool.mouse.click", "config": {"button": "left"}}],
		"mouse_right_click": [{"action": "xdotool.mouse.click", "config": {"button": "right"}}],
		
		"volume_up": [{"action": "xdotool.keyboard.key", "config": {"key": "XF86AudioRaiseVolume"}}],
		"volume_down": [{"action": "xdotool.keyboard.key", "config": {"key": "XF86AudioLowerVolume"}}],
		"mute": [{"action": "xdotool.keyboard.key", "config": {"key": "XF86AudioMute"}}],
		"up_arrow": [{"action": "xdotool.keyboard.key", "config": {"key": "Up"}}],
		"down_arrow": [{"action": "xdotool.keyboard.key", "config": {"key": "Down"}}],
		
		"one": [{"action": "xdotool.keyboard.key", "config": {"key": "1"}}],
		"two": [{"action": "xdotool.keyboard.key", "config": {"key": "2"}}],
		"three": [{"action": "xdotool.keyboard.key", "config": {"key": "3"}}],
		"four": [{"action": "xdotool.keyboard.key", "config": {"key": "4"}}],
		"five": [{"action": "xdotool.keyboard.key", "config": {"key": "5"}}],
		"six": [{"action": "xdotool.keyboard.key", "config": {"key": "6"}}],
		"seven": [{"action": "xdotool.keyboard.key", "config": {"key": "7"}}],
		"eight": [{"action": "xdotool.keyboard.key", "config": {"key": "8"}}],
		"nine": [{"action": "xdotool.keyboard.key", "config": {"key": "9"}}],
		"zero": [{"action": "xdotool.keyboard.key", "config": {"key": "0"}}],
		"star": [{"action": "xdotool.keyboard.key", "config": {"key": "asterisk"}}],
		"pound": [{"action": "xdotool.keyboard.key", "config": {"key": "numbersign"}}],
		"enter": [{"action": "xdotool.keyboard.key", "config": {"key": "Return"}}],
		
		"kodi": [{"action": "launcher.launch", "config": {"executable": "kodi", "args":["-fs"]}}],
		
		"away": [{"action": "purple.setstatus", "config": {"status": "away"}}],
		"available": [{"action": "purple.setstatus", "config": {"status": "available"}}],
		"osd": [{"action": "osd.display.text", "config": {"text": "$event -> $macro -> $action"}}]
	}
}
