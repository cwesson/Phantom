"""
Phantom Plugin - Phantom Instructions
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import os

class Phi:
	def __init__(self, el, act):
		self.act = act
		self.el = el
		self.act.register("phi.set", self.set)
		self.act.register("phi.get", self.get)
		self.act.register("phi.unset", self.unset)
		self.act.register("phi.echo", self.echo)
		self.act.register("phi.event", self.event)
		self.act.register("phi.if", self.cond)
		self.act.register("phi.switch", self.switch)
		self.act.register("phi.equal", self.equal)
		self.act.register("phi.notequal", self.notequal)
		self.act.register("phi.add", self.add)
		self.act.register("phi.subtract", self.subtract)
		self.act.register("phi.multiply", self.multiply)
		self.act.register("phi.divide", self.divide)
		self.act.register("phi.modulo", self.modulo)
		self.vars = {}
	
	def deactivate(self):
		self.act.unregister("phi.set")
		self.act.unregister("phi.get")
		self.act.unregister("phi.unset")
		self.act.unregister("phi.echo")
		self.act.unregister("phi.event")
		self.act.unregister("phi.if")
		self.act.unregister("phi.switch")
		self.act.unregister("phi.equal")
		self.act.unregister("phi.notequal")
		self.act.unregister("phi.add")
		self.act.unregister("phi.subtract")
		self.act.unregister("phi.multiply")
		self.act.unregister("phi.divide")
		self.act.unregister("phi.modulo")
	
	def set(self, arg):
		if "key" in arg and "value" in arg:
			self.vars[arg["key"]] = arg["value"]
		return 0
	
	def get(self, arg):
		if "key" in arg:
			if arg["key"] in self.vars:
				return self.vars[arg["key"]]
		return None
	
	def unset(self, arg):
		if "key" in arg:
			if arg["key"] in self.vars:
				del self.vars[arg["key"]]
		return 0
	
	def echo(self, arg):
		if "echo" in arg:
			return arg["echo"]
		return None
	
	def event(self, arg):
		if "event" in arg:
			self.el.event(arg["event"])
			return 0
		return None
	
	def cond(self, arg):
		if "if" in arg:
			ret = int(arg["if"])
			if ret:
				if "true" in arg:
					self.el.event(arg["true"])
			else:
				if "false" in arg:
					self.el.event(arg["false"])
			return ret
		return None
	
	def switch(self, arg):
		if "switch" in arg:
			value = arg["switch"]
			if "case" in arg:
				for case in arg["case"]:
					if value == case:
						self.el.event(arg["case"][case])
						return value
			if "default" in arg:
				self.el.event(arg["default"])
				return value
		return None
	
	def equal(self, arg):
		if "left" in arg and "right" in arg:
			return arg["left"] == arg["right"]
		return False
	
	def notequal(self, arg):
		if "left" in arg and "right" in arg:
			return not arg["left"] == arg["right"]
		return False
	
	def add(self, arg):
		if "left" in arg and "right" in arg:
			return float(arg["left"]) + float(arg["right"])
		return 0
	
	def subtract(self, arg):
		if "left" in arg and "right" in arg:
			return float(arg["left"]) - float(arg["right"])
		return 0
	
	def multiply(self, arg):
		if "left" in arg and "right" in arg:
			return float(arg["left"]) * float(arg["right"])
		return 0
	
	def divide(self, arg):
		if "left" in arg and "right" in arg:
			return float(arg["left"]) / float(arg["right"])
		return 0
	
	def modulo(self, arg):
		if "left" in arg and "right" in arg:
			return float(arg["left"]) % float(arg["right"])
		return 0
	
def activate(el, act, args):
	return Phi(el, act)
