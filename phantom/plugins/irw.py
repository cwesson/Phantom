"""
Phantom Plugin - LIRCD/IRW
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import subprocess
import threading

class IRWEventProducer(threading.Thread):
	def __init__(self, el, act):
		threading.Thread.__init__(self)
		self.el = el
		self.start()
		self.irw = None
	
	def run(self):
		self.irw = subprocess.Popen(["irw"], stdout=subprocess.PIPE)
		for line in iter(self.irw.stdout.readline, ''):
			parts = line.strip().split(b' ')
			if len(parts) >= 3:
				event = '.'.join(["irw", parts[2].decode('utf-8')])
				self.el.event(event)
			else:
				break
	
	def deactivate(self):
		if self.irw is not None:
			self.irw.kill()

def activate(el, act, args):
	if 'start' in args:
		subprocess.Popen(["service", "lircd", "start"])
	return IRWEventProducer(el, act)

