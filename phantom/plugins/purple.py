"""
Phantom Plugin - libpurple
@author Conlan Wesson
@copyright (C) 2019 Conlan Wesson
"""

import subprocess

class PurpleRemote:
	def __init__(self, el, act):
		self.el = el
		self.act = act
		self.act.register("purple.setstatus", self.setStatus)
	
	def deactivate(self):
		self.act.unregister("purple.setstatus")
	
	def setStatus(self, arg):
		if "message" not in arg:
			arg["message"] = ""
		child = subprocess.Popen(["purple-remote", "setstatus?status={}&message={}".format(arg["status"], arg["message"])])
		child.communicate()[0]
		return child.returncode

def activate(el, act, args):
	return PurpleRemote(el, act)
