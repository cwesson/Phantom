"""
Phantom Plugin - xdotool
@author Conlan Wesson
@copyright (C) 2019 Conlan Wesson
"""

import subprocess

class Keyboard:
	def __init__(self, el, act):
		self.el = el
		self.act = act
		self.act.register("xdotool.keyboard.key", self.key)
		self.act.register("xdotool.keyboard.type", self.type)
	
	def deactivate(self):
		self.act.unregister("xdotool.keyboard.type")
	
	def key(self, arg):
		child = subprocess.Popen(["xdotool", "key", arg["key"]], stdout=subprocess.PIPE)
		child.communicate()[0]
		return child.returncode
	
	def type(self, arg):
		delay = 12
		if "delay" in arg:
			delay = int(arg["delay"])
		child = subprocess.Popen(["xdotool", "type", "--delay", str(delay), arg["text"]])
		child.communicate()[0]
		return child.returncode

class Mouse:
	def __init__(self, el, act):
		self.el = el
		self.act = act
		self.act.register("xdotool.mouse.move", self.move)
		self.act.register("xdotool.mouse.click", self.click)
	
	def deactivate(self):
		self.act.unregister("xdotool.mouse.move")
		self.act.unregister("xdotool.mouse.click")
	
	def move(self, arg):
		child = subprocess.Popen(["xdotool", "mousemove_relative", "--", arg["x"], arg["y"]])
		child.communicate()[0]
		return child.returncode

	def click(self, arg):
		if arg["button"] == "left":
			button = "1"
		elif arg["button"] == "middle":
			button = "2"
		elif arg["button"] == "right":
			button = "3"
		repeat = 1
		if "repeat" in arg:
			repeat = int(arg["repeat"])
		delay = 12
		if "delay" in arg:
			delay = int(arg["delay"])
		child = subprocess.Popen(["xdotool", "click", "--repeat", str(repeat), button])
		child.communicate()[0]
		return child.returncode

class Xdotool:
	def __init__(self, el, act):
		self.key = Keyboard(el, act)
		self.mouse = Mouse(el, act)
	
	def deactivate(self):
		self.key.deactivate()
		self.mouse.deactivate()

def activate(el, act, args):
	return Xdotool(el, act)
