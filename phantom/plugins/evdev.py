"""
Phantom Plugin - EvDev
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import evdev
import threading

class EvDev(threading.Thread):
	KEYEVENTS = ["up", "down", "left", "right", "a", "b", "x", "y", "l", "r", "select", "start"]
	def __init__(self, el, act, args):
		threading.Thread.__init__(self)
		self.el = el
		self._stop_event = threading.Event()
		self.gamepad = evdev.InputDevice(args['dev'])
		self.keymap = {}
		for key in EvDev.KEYEVENTS:
			if key in args:
				if args[key] not in self.keymap:
					self.keymap[args[key]] = []
				self.keymap[args[key]].append(key)
		self.start()
	
	def run(self):
		while not self._stop_event.is_set():
			for event in self.gamepad.read_loop():
				if event.type == evdev.ecodes.EV_KEY or event.type == evdev.ecodes.EV_ABS:
					etype = event.type
					ecode = event.code
					evalue = event.value
					if event.type == evdev.ecodes.EV_KEY:
						etype = "key"
						if event.value:
							evalue = "down"
						else:
							evalue = "up"
					elif event.type == evdev.ecodes.EV_ABS:
						etype = "abs"
						if event.code == 0:
							ecode = "x"
						elif event.code == 1:
							ecode = "y"
					ev = "{}.{}".format(etype, ecode)
					if ev in self.keymap:
						for key in self.keymap[ev]:
							self.el.event("evdev.{}.{}".format(key, evalue))
					else:
						ev = "{}.{}".format(ev, evalue)
						if ev in self.keymap:
							for key in self.keymap[ev]:
								self.el.event("evdev.{}".format(key))
						else:
							self.el.event("evdev.{}".format(ev))
	
	def deactivate(self):
		self._stop_event.set()

def activate(el, act, args):
	return EvDev(el, act, args)

