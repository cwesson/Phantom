"""
Phantom Plugin - Timer
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import threading
import time

class Timer:
	def __init__(self, el, act):
		self.act = act
		self.el = el
		self.act.register("timer.set", self.set)
		self.act.register("timer.cancel", self.cancel)
		self.act.register("timer.delay", self.delay)
		self.timers = {}
	
	def deactivate(self):
		for t in self.timers:
			t.cancel()
		self.act.unregister("timer.set")
		self.act.unregister("timer.cancel")
		self.act.unregister("timer.delay")
	
	def set(self, arg):
		if "name" in arg and "duration" in arg:
			if arg["name"] in self.timers:
				self.timers[arg["name"]].cancel()
				self.timers.pop(arg["name"])
			timer = threading.Timer(float(arg["duration"]), self._expired, args=[arg["name"]])
			timer.start()
			self.timers[arg["name"]] = timer
		return 0
	
	def _expired(self, name):
		self.timers[name].cancel()
		self.timers.pop(name)
		self.el.event("timer.{}".format(name))
	
	def cancel(self, arg):
		if "name" in arg:
			name = arg["name"]
			if name in self.timers:
				self.timers[name].cancel()
				self.timers.pop(name)
		return 0
	
	def delay(self, arg):
		if "duration" in arg:
			time.sleep(float(arg["duration"]))
		return 0

def activate(el, act, args):
	return Timer(el, act)
