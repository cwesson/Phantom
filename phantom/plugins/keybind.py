"""
Phantom Plugin - Keybind
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import keybind

class Keybind:
	def __init__(self, el, act, args):
		self.act = act
		self.el = el
		make_func = lambda k: lambda: self._keycall(k)
		if 'keys' in args:
			keylist = {}
			for key in args['keys']:
				keylist[key] = make_func(key)
			keybind.KeyBinder.activate(keylist, run_thread=True)
	
	def deactivate(self):
		pass
	
	def _keycall(self, key):
		self.el.event("keybind.key.{}".format(key))
	
def activate(el, act, args):
	return Keybind(el, act, args)
