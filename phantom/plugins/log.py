"""
Phantom Plugin - Log
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import logging
import threading
import time

class Log:
	def __init__(self, el, act):
		self.act = act
		self.el = el
		self.log = logging.getLogger('Log')
		self.act.register("log.debug", self.debug)
		self.act.register("log.info", self.info)
		self.act.register("log.warning", self.warning)
		self.act.register("log.error", self.error)
		self.act.register("log.critical", self.critical)
	
	def deactivate(self):
		self.act.unregister("log.debug")
		self.act.unregister("log.info")
		self.act.unregister("log.warning")
		self.act.unregister("log.error")
		self.act.unregister("log.critical")
	
	def debug(self, arg):
		if "msg" in arg:
			self.log.debug(arg["msg"])
		return 0
	
	def info(self, arg):
		if "msg" in arg:
			self.log.info(arg["msg"])
		return 0
	
	def warning(self, arg):
		if "msg" in arg:
			self.log.warning(arg["msg"])
		return 0
	
	def error(self, arg):
		if "msg" in arg:
			self.log.error(arg["msg"])
		return 0
	
	def critical(self, arg):
		if "msg" in arg:
			self.log.critical(arg["msg"])
		return 0

def activate(el, act, args):
	return Log(el, act)
