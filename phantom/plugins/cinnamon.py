"""
Phantom Plugin - Cinnamon Desktop
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import subprocess
import threading
import time

class Cinnamon(threading.Thread):
	def __init__(self, el, act):
		threading.Thread.__init__(self)
		self.el = el
		self.running = True
		self.locked = False
		self.start()
	
	def run(self):
		while(self.running):
			ss = subprocess.Popen(["cinnamon-screensaver-command", "-q"], stdout=subprocess.PIPE)
			for line in iter(ss.stdout.readline, b''):
				if line.find(b"screensaver is active") >= 0 and not self.locked:
					self.locked = True
					self.el.event("cinnamon.lock")
				elif line.find(b"screensaver is inactive") >= 0 and self.locked:
					self.locked = False
					self.el.event("cinnamon.unlock")
			
			time.sleep(5)
	
	def deactivate(self):
		self.running = False

def activate(el, act, args):
	return Cinnamon(el, act)

