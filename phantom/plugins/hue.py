"""
Phantom Plugin - Hue
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import json
import logging
import requests
import urllib.parse

class Hue:
	def __init__(self, act, args):
		self.act = act
		self.log = logging.getLogger('Hue')
		self.api = 'http://{}/api/{}'.format(args['domain'], args['username'])
		self.act.register('hue.set', self.lightset)
		self.act.register('hue.on', self.lighton)
		self.act.register('hue.off', self.lightoff)
	
	def lightset(self, args):
		on = True
		sat = 254
		bri = 254
		hue = 35000
		if 'on' in args:
			on = bool(args['on'])
		if 'saturation' in args:
			sat = int(args['saturation'])
		if 'brightness' in args:
			bri = int(args['brightness'])
		if 'hue' in args:
			hue = int(args['hue'])
		
		body = {'on':on, 'sat':sat, 'bri':bri, 'hue':hue}
		
		if 'group' in args:
			group = self.getGroupID(args['group'])
			if 'scene' in args:
				sid = self.findScene(group, args['scene'])
				body = {'scene': sid}
			body['transitiontime'] = 0
			self.requestGroup(group, body)
		
		if 'lights' in args:
			for light in args['lights']:
				self.requestLight(light, body)
	
	def lighton(self, args):
		sat = 254
		bri = 254
		if 'brightness' in args:
			bri = int(args['brightness'])
		
		if 'group' in args:
			group = self.getGroupID(args['group'])
			self.requestGroup(group, {'on':on, 'sat':sat, 'bri':bri, 'transitiontime':0})
		
		if 'lights' in args:
			for light in args['lights']:
				self.requestLight(light, {'on':True, 'sat':sat, 'bri':bri})
	
	def lightoff(self, args):
		if 'group' in args:
			group = self.getGroupID(args['group'])
			self.requestGroup(group, {'on':False,})
		
		if 'lights' in args:
			for light in args['lights']:
				self.requestLight(light, {'on':False})
	
	def _putRequest(self, url, body):
		r = requests.put(url, body)
		j = r.json()[0]
		if 'error' in j:
			self.log.error(j['error']['description'])
		return j
	
	def _getRequest(self, url):
		r = requests.get(url)
		j = r.json()
		if 'error' in j:
			self.log.error(j['error']['description'])
		return j
	
	def requestLight(self, light, body):
		self._putRequest('{}/lights/{}/state'.format(self.api, light), json.dumps(body))
	
	def requestGroup(self, group, body):
		self._putRequest('{}/groups/{}/action'.format(self.api, group), json.dumps(body))
	
	def getGroupID(self, group):
		if type(group) is int:
			return group
		elif type(group) is float:
			return int(group)
		elif type(group) is str:
			j = self._getRequest('{}/groups'.format(self.api))
			for gid in j:
				if j[gid]['name'] == group:
					return int(gid)
		return 0
	
	def findScene(self, group, scene):
		j = self._getRequest('{}/scenes'.format(self.api))
		if scene in j:
			return scene
		else:
			for sid in j:
				if j[sid]['group'] == str(group) and j[sid]['name'] == scene:
					return sid
		return ""
	
	def deactivate(self):
		self.act.unregister('hue.set')

def activate(el, act, args):
	return Hue(act, args)

