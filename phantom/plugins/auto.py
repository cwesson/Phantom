"""
Phantom Plugin - auto
@author Conlan Wesson
@copyright (C) 2019 Conlan Wesson
"""

import pyautogui
import time

class Keyboard:
	def __init__(self, el, act):
		self.el = el
		self.act = act
		self.act.register("auto.keyboard.key", self.key)
		self.act.register("auto.keyboard.type", self.type)
	
	def deactivate(self):
		self.act.unregister("auto.keyboard.type")
	
	def key(self, arg):
		pyautogui.press(arg["key"])
		return 0
	
	def type(self, arg):
		delay = 12
		if "delay" in arg:
			delay = int(arg["delay"])
		for key in arg["text"]:
			pyautogui.write([key])
			time.sleep(delay/1000)
		return 0

class Mouse:
	def __init__(self, el, act):
		self.el = el
		self.act = act
		self.act.register("auto.mouse.move", self.move)
		self.act.register("auto.mouse.click", self.click)
	
	def deactivate(self):
		self.act.unregister("auto.mouse.move")
		self.act.unregister("auto.mouse.click")
	
	def move(self, arg):
		pyautogui.move(int(arg["x"]), int(arg["y"]))
		return 0

	def click(self, arg):
		repeat = 1
		if "repeat" in arg:
			repeat = int(arg["repeat"])
		delay = 12
		if "delay" in arg:
			delay = int(arg["delay"])
		pyautogui.click(button=arg["button"], clicks=repeat, interval=delay/1000)
		return 0

class Auto:
	def __init__(self, el, act):
		self.key = Keyboard(el, act)
		self.mouse = Mouse(el, act)
	
	def deactivate(self):
		self.key.deactivate()
		self.mouse.deactivate()

def activate(el, act, args):
	return Auto(el, act)
