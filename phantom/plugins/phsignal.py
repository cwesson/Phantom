"""
Phantom Plugin - signal
@author Conlan Wesson
@copyright (C) 2019 Conlan Wesson
"""

import signal

class Signal:
	def __init__(self, el, act, sigs):
		self.SIGNALS = sigs
		self.el = el
		self.act = act
		for s in self.SIGNALS:
			signal.signal(s, self.sighandler)
		self.act.register('signal.alarm', self.alarm)
	
	def deactivate(self):
		self.act.unregister('signal.alarm')
		for s in self.SIGNALS:
			signal.signal(s, signal.SIG_DFL)
	
	def alarm(self, arg):
		if 'time' in arg:
			signal.alarm(int(arg['time']))
			return 0
		else:
			return 1
	
	def sighandler(self, signum, frame):
		if signum in self.SIGNALS:
			self.el.event('signal.{}'.format(self.SIGNALS[signum]))

def activate(el, act, args):
	SIGNALS = {
		'interrupt': signal.SIGINT,
		'quit': signal.SIGQUIT,
		'abort': signal.SIGABRT,
		'terminate': signal.SIGTERM,
		'user1': signal.SIGUSR1,
		'user2': signal.SIGUSR2,
		'alarm': signal.SIGALRM
	}
	sigs = {}
	if "signals" in args:
		for s in args["signals"]:
			if s in SIGNALS:
				sigs[SIGNALS[s]] = s
	else:
		for s in SIGNALS:
			sigs[SIGNALS[s]] = s
	return Signal(el, act, sigs)
