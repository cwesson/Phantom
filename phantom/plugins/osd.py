"""
Phantom Plugin - OSD
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import subprocess
import threading

class OSDThread(threading.Thread):
	def __init__(self, arg):
		threading.Thread.__init__(self)
		self.arg = arg
		self.start()
	
	def run(self):
		color = "green"
		if "color" in self.arg:
			color = self.arg["color"]
		font = "10x20"
		if "font" in self.arg:
			font = self.arg["font"]
		hpos = "center"
		if "hpos" in self.arg:
			hpos = self.arg["hpos"]
		hoffset = 0
		if "hoffset" in self.arg:
			hoffset = self.arg["hoffset"]
		vpos = "middle"
		if "vpos" in self.arg:
			vpos = self.arg["vpos"]
		voffset = 0
		if "voffset" in self.arg:
			voffset = self.arg["voffset"]
		args = [
			"osd_cat", "--color", color, "--font", font,
			"--align", hpos, "--indent", str(hoffset),
			"--pos", vpos, "--offset", str(voffset)
		]
		self.process(args)

class OSDText(OSDThread):
	def __init__(self, arg):
		OSDThread.__init__(self, arg)
	
	def process(self, args):
		osd = subprocess.Popen(args, stdin=subprocess.PIPE)
		osd.communicate(input=self.arg["text"].encode('utf-8'))

class OSDBar(OSDThread):
	def __init__(self, arg):
		OSDThread.__init__(self, arg)
	
	def process(self, args):
		args.extend(["--barmode", "bar", "--percentage", str(self.arg["percent"])])
		if "text" in self.arg:
			args.extend(["--text", self.arg["text"]])
		osd = subprocess.Popen(args, stdin=subprocess.PIPE)

class OSDSlider(OSDThread):
	def __init__(self, arg):
		OSDThread.__init__(self, arg)
	
	def process(self, args):
		args.extend(["--barmode", "slider", "--percentage", str(self.arg["percent"])])
		if "text" in self.arg:
			args.extend(["--text", self.arg["text"]])
		osd = subprocess.Popen(args, stdin=subprocess.PIPE)

class OSD:
	def __init__(self, el, act):
		self.el = el
		self.act = act
		self.act.register("osd.display.text", self.text)
		self.act.register("osd.display.bar", self.bar)
		self.act.register("osd.display.slider", self.slider)
	
	def deactivate(self):
		self.act.unregister("osd.display.text")
		self.act.unregister("osd.display.bar")
		self.act.unregister("osd.display.slider")
	
	def text(self, arg):
		# Spawn the text in a thread so as not to hold up the rest of the macro
		OSDText(arg)
		return 0
	
	def bar(self, arg):
		# Spawn the text in a thread so as not to hold up the rest of the macro
		OSDBar(arg)
		return 0
	
	def slider(self, arg):
		# Spawn the text in a thread so as not to hold up the rest of the macro
		OSDSlider(arg)
		return 0

def activate(el, act, args):
	return OSD(el, act)
