"""
Phantom Plugin - Launcher
@author Conlan Wesson
@copyright (C) 2019 Conlan Wesson
"""

import os

class Launcher:
	def __init__(self, act):
		self.act = act
		self.act.register("launcher.launch", self.launch)
	
	def deactivate(self):
		self.act.unregister("launcher.launch")
	
	def launch(self, arg):
		if "executable" in arg:
			if os.fork() == 0:
				os.setsid()
				if "args" not in arg:
					arg["args"] = [""]
				arg["args"].insert(0, arg["executable"])
				os.execvp(arg["executable"], arg["args"])
			return 0
		else:
			return 1
	
def activate(el, act, args):
	return Launcher(act)
