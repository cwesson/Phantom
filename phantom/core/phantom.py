#!/usr/bin/env python
"""
Phantom
Event driven macro runner for Linux.
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import argparse
import json
import logging
import pathlib
import sys
import time
import traceback

from . import actions
from . import events
from . import macros
from . import plugins

__author__    = "Conlan Wesson"
__copyright__ = "Copyright (C) 2020"
__license__   = "GPLv3"

class Phantom:
	def __init__(self):
		self.running = True

	def exit(self, arg):
		self.running = False

	def activate(self, el, act):
		self.act.register("phantom.exit", self.exit)

	def deactivate(self):
		self.act.unregister("phantom.exit")

	def main(self):
		argparser = argparse.ArgumentParser(description='Event driven macro runner for Linux.')
		argparser.add_argument('file', type=str, nargs='?', help="Phantom config file.")
		argparser.add_argument('-p', '--plugins', metavar="PLUGIN,ARGS", type=str, nargs='*', default={}, help="Arguments to pass to plugins.")
		argparser.add_argument('-j', '--jobs', type=int, help="Number of jobs to run simultaneously.")
		argparser.add_argument('-v', '--verbose', action='store_true', default=False, help="Show debug messages.")
		args = argparser.parse_args()

		# Start logger
		logFormatter = logging.Formatter("%(asctime)s %(levelname)s %(name)s: %(message)s")
		rootlog = logging.getLogger()
		rootlog.setLevel(logging.NOTSET)

		consoleLog = logging.StreamHandler(sys.stdout)
		consoleLog.setFormatter(logFormatter)
		consoleLog.setLevel(logging.INFO)
		rootlog.addHandler(consoleLog)

		errorLog = logging.StreamHandler(sys.stderr)
		errorLog.setFormatter(logFormatter)
		errorLog.setLevel(logging.WARNING)
		rootlog.addHandler(errorLog)

		log = logging.getLogger('Phantom')
		log.info("{}  {}".format(__copyright__, __author__))

		# Load config
		if args.file is None:
			# Try loading file from working directory
			rc = ".phantomrc"
			rcfile = pathlib.Path(rc)
			if rcfile.is_file():
				args.file = rc
			else:
				# Try loading file from home directory
				rc = "{}/.phantomrc".format(str(pathlib.Path.home()))
				rcfile = pathlib.Path(rc)
				if rcfile.is_file():
					args.file = rc
		with open(args.file) as data:
			config = json.load(data)
		
		verbose = args.verbose
		if not verbose and "verbose" in config["options"]:
			verbose = config["options"]["verbose"]
		if verbose:
			consoleLog.setLevel(logging.DEBUG)

		# Start core services
		self.act = actions.ActionList()

		if args.jobs is not None:
			jobs = args.jobs
		else:
			jobs = config["options"]["jobs"]
		runner = macros.MacroRunner(self.act, jobs)
		for macro in config["macros"]:
			runner.addMacro(macro, config["macros"][macro])

		self.el = events.EventListener(runner)
		for event in config["events"]:
			for macro in config["events"][event]:
				self.el.addListener(event, macro)
		self.act.setEventListener(self.el)
		runner.setEventListener(self.el)
		self.el.start()

		# Load plugins
		self.activate(self.el, self.act)

		pm = plugins.PluginManager(self.el, self.act)
		if "plugins" in config["options"]:
			pluginargs = config["options"]["plugins"]
		else:
			pluginargs = {}
		for p in args.plugins:
			arg = p.split(',')
			pluginargs[arg[0]] = arg[1:]
		for p in config["plugins"]:
			if p in pluginargs:
				arg = pluginargs[p]
			else:
				arg = {}
			pm.activate(p, arg)

		# Wait to be killed
		self.el.event('phantom.running')
		while self.running:
			try:
				while self.running:
					time.sleep(1)
			except KeyboardInterrupt:
				self.el.event('phantom.interrupt')
			except:
				log.error(traceback.format_exc())
				break
		self.el.event('phantom.stopping')
		self.el.flush()

		# Stop plugins
		pm.deactivateAll()

		self.deactivate()

		# Stop event listener
		self.el.stop()

def main():
	ph = Phantom()
	ph.main()

if __name__ == "__main__":
	main()
