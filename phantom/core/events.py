"""
Phantom Event Listener
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import logging
import threading
import queue

class EventQueue(threading.Thread):
	"""
	Queue for pending events.
	"""
	START_EVENT = "phantom.start"
	EXIT_EVENT = "phantom.exit"
	def __init__(self, runner):
		"""
		Constructor
		@param runner MacroRunner to use when events are received.
		"""
		threading.Thread.__init__(self)
		self.runner = runner
		self.events = {}
		self.q = queue.Queue()
		self.log = logging.getLogger('Event')
		self.put(self.START_EVENT)
	
	def addListener(self, event, macro):
		"""
		Add a macro to run when an event is received.
		@param event Event to listen for.
		@param macro Macro to run when the event is received.
		"""
		if event not in self.events:
			self.events[event] = []
		self.events[event].append(macro)
	
	def removeListener(self, event, macro):
		"""
		Stop listening for an event.
		@param event Event to stop listening for.
		@param macro Macro to stop running for the given event.
		"""
		if event in self.events:
			if macro in self.events[event]:
				self.events[event].remove(macro)
	
	def put(self, e):
		"""
		Enqueue an event for processing
		@param e Event to enqueue.
		"""
		self.q.put(e)
	
	def run(self):
		"""
		Thread to process events.
		"""
		while True:
			e = self.q.get()
			self.log.info(e)
			env = {"event": e}
			# Search for all wildcards
			self.__execute("*", env)
			parts = e.split(".")
			ev = ""
			for p in parts:
				# Generate next pattern
				if ev == "":
					ev = p
				else:
					ev = ".".join([ev,p])
				if ev == e:
					search = ev
				else:
					search = "{}.*".format(ev)
				self.__execute(search, env)
			
			# Special case, exit causes the queue thread to end
			if e == self.EXIT_EVENT:
				break
		
		self.runner.stop()
	
	def __execute(self, name, env):
		self.log.debug("Wildcard: {}".format(name))
		# Check for registered macros
		if name in self.events:
			for macro in self.events[name]:
				# Dispatch registered macro to the MacroRunner
				self.log.debug("Macro: {}".format(macro))
				self.runner.run(macro, env)
	
	def stop(self):
		"""
		Stop the event processing thread.
		"""
		self.put(self.EXIT_EVENT)
	
	def flush(self):
		"""
		Flush the event processing thread.
		"""
		while not self.q.empty():
			pass

class EventListener:
	"""
	Service for receiving events
	"""
	def __init__(self, runner):
		"""
		Constructor
		@param runner MacroRunner to use when events are received.
		"""
		self.q = EventQueue(runner)
	
	def event(self, e):
		"""
		Receive an event.
		@param e Incoming event name.
		"""
		self.q.put(e)
	
	def stop(self):
		"""
		Stop the EventListener
		"""
		self.flush()
		self.q.stop()
	
	def start(self):
		"""
		Start the EventListener
		"""
		self.q.start()
	
	def flush(self):
		"""
		Flush the EventListener
		"""
		self.q.flush()
	
	def addListener(self, event, macro):
		"""
		Add a macro to run when an event is received.
		@param event Event to listen for.
		@param macro Macro to run when the event is received.
		"""
		self.q.addListener(event, macro)
		
	def removeListener(self, event, macro):
		"""
		Stop listening for an event.
		@param event Event to stop listening for.
		@param macro Macro to stop running for the given event.
		"""
		self.q.removeListener(event, macro)

