"""
Phantom Action List
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import logging

class ActionList:
	"""
	List of available actions.
	"""
	def __init__(self):
		"""
		Constructor
		"""
		self.el = None
		self.actions = {}
		self.log = logging.getLogger('Action')
	
	def setEventListener(self, el):
		self.el = el
	
	def register(self, name, cb):
		"""
		Add an action.
		@param name Name of the action to add.
		@param cb Function to call to execute the action.
		"""
		self.actions[name] = cb
		self.log.debug("Action {} registered".format(name))
	
	def unregister(self, name):
		"""
		Remove an action.
		@param name Name of the action to remove.
		"""
		self.actions.pop(name)
		self.log.debug("Action {} unregistered".format(name))
	
	def do(self, name, args, env):
		"""
		Execute an action.
		@param name Name of the action to execute.
		@param args Arguments to pass to the action.
		@param env Variables to substitute.
		"""
		if name in self.actions:
			self.log.info(name)
			# Substitute variables
			expanded = args.copy()
			for arg in expanded.keys():
				if isinstance(expanded[arg], str):
					for v in env:
						expanded[arg] = expanded[arg].replace("${}".format(v), env[v])
			# Run the action
			if not self.el is None:
				self.el.event("phantom.action.{}".format(name))
			return self.actions[name](expanded)
		else:
			self.log.warning("No such action \"{}\"".format(name))
