"""
Phantom Plugin Manager
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import importlib
import logging
import sys

sys.path.insert(1, "../plugins")

class PluginManager:
	"""
	Phantom plugin manager
	"""
	def __init__(self, el, act):
		"""
		Constructor
		@param el EventListener to pass to plugins.
		@param act ActionList to pass to plugins.
		"""
		self.el = el
		self.act = act
		self.log = logging.getLogger('Plugin')
		self.plugins = {}
	
	def activate(self, plugin, args):
		"""
		Activate a plugin.
		@param plugin Name of the plugin to activate.
		"""
		try:
			self.plugins[plugin] = (importlib.import_module('phantom.plugins.{}'.format(plugin)).activate(self.el, self.act, args))
			self.el.event("phantom.activate.{}".format(plugin))
		except ImportError:
			self.log.error("Could not activate plugin {}".format(plugin))
	
	def deactivate(self, plugin):
		"""
		Deactivate a plugin.
		@param plugin Name of the plugin to deactivate.
		"""
		if plugin in self.plugins:
			self.el.event("phantom.deactivate.{}".format(plugin))
			self.el.flush()
			self.plugins[plugin].deactivate()
			del self.plugins[plugin]
		else:
			self.log.warning("Plugin {} not active".format(plugin))
	
	def deactivateAll(self):
		"""
		Deactivate all active plugins.
		"""
		keys = list(self.plugins.keys())
		for p in keys:
			self.deactivate(p)

