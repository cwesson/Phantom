"""
Phantom Macro Runner
@author Conlan Wesson
@copyright (C) 2020 Conlan Wesson
"""

import logging
import threading
import queue

class Macro:
	"""
	Macro definition.
	"""
	def __init__(self, name, env=None):
		"""
		Constructor
		@param name Name of the macro.
		@param env Variables for the macro.
		"""
		self.name = name
		self.setEnv(env)
		self.actions = []
	
	def addAction(self, action):
		"""
		Append an action to the end of the macro.
		@param action Action to add.
		"""
		self.actions.append(action)
	
	def getActions(self):
		"""
		Get the list of actions.
		@return List of actions.
		"""
		return self.actions
	
	def getEnv(self):
		"""
		Get the list of variables.
		@return List of variables.
		"""
		return self.env
	
	def setEnv(self, env):
		if env:
			self.env = env.copy()
		else:
			self.env = {}


class MacroWorker(threading.Thread):
	"""
	Macro worker thread.
	"""
	EXIT_MACRO = "phantom.exit"
	
	def __init__(self, runner, act):
		"""
		Constructor
		@param runner Master MacroRunner.
		@param act ActionList to execute actions.
		"""
		threading.Thread.__init__(self)
		self.runner = runner
		self.act = act
		self.el = None
		self.log = logging.getLogger('Macro')
		self.start()
	
	def setEventListener(self, el):
		self.el = el
	
	def run(self):
		"""
		Macro execution thread.
		"""
		while True:
			# Wait for a macro to run
			macro = self.runner.next()
			
			# Special case, exit causes the thread to end
			if macro == self.EXIT_MACRO:
				break
			else:
				self.log.debug(macro.name)
				if not self.el is None:
					self.el.event("phantom.macro.{}".format(macro.name))
				env = macro.getEnv()
				env["macro"] = macro.name
				prev = None
				# Execute each action
				for action in macro.getActions():
					if "action" in action:
						# Update the variables
						env["action"] = action["action"]
						env["prev"] = "{}".format(prev)
						# Execute
						if "config" in action:
							config = action["config"]
						else:
							config = {}
						prev = self.act.do(action["action"], config, env)
						self.log.debug("{}: {}".format(action["action"], prev))
					else:
						self.log.error("Missing action in macro \"{}\"".format(macro.name))

class MacroRunner:
	"""
	Macro runner.
	"""
	def __init__(self, act, num):
		"""
		Constructor
		@param act ActionList to execute actions from.
		@param num Number of worker threads.
		"""
		self.q = queue.Queue()
		self.macros = {}
		self.workers = []
		self.log = logging.getLogger('Macro')
		# Start worker threads
		for i in range(num):
			self.workers.append(MacroWorker(self, act))
	
	def setEventListener(self, el):
		for worker in self.workers:
			worker.setEventListener(el)
	
	def run(self, name, env=None):
		"""
		Run a macro.
		@param name Name of the macro to run.
		"""
		if name in self.macros:
			# Enqueue the request to run
			if env:
				self.macros[name].setEnv(env)
			self.q.put(self.macros[name])
		else:
			self.log.warning("No such macro \"{}\"".format(name))
	
	def next(self):
		"""
		Get the next Macro in the queue.
		@return Next Macro.
		"""
		return self.q.get()
	
	def stop(self):
		"""
		Stop the MacroRunner.
		"""
		# Enqueue enough exit macros to stop all the workers
		for worker in self.workers:
			self.q.put(MacroWorker.EXIT_MACRO)
	
	def addMacro(self, name, actions):
		"""
		Add a new macro.
		@param name Name of the new macro.
		@param actions List of actions in the macro.
		"""
		macro = Macro(name)
		for act in actions:
			macro.addAction(act)
		self.macros[name] = macro
	
	def removeMacro(self, name):
		"""
		Remove a macro.
		@param macro Name of the macro to remove.
		"""
		if name in self.macros:
			del self.macros[name]
