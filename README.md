# Phantom
Event driven macro runner for Linux.

## Overview
Phantom is an event driven macro runner for Linux.  This is done through three
key components: events, macros, and actions.  Phantom listens for events from
other applications.  When a recognized event occurs, Phantom executes a series
of macros registered to the given event.  Each macro contains a sequential list
of actions that perform the desired functionality.

## License
GPLv3 - See LICENSE

Copyright (C) 2019 Conlan Wesson

## Plugins
Most of the utility in Phantom is provided through plugins.  Plugins may consist
of events, actions, or both.  In order to load plugins, they must be located in
the "plugins" directory in the Phantom install path.  Which plugins are loaded
is specified in the phantomrc

### Events
Plugins may run the background listening for events from outside of Phantom.
When an event is detected, the plugin sends an event to the Phantom core.  Each
event is a dot seperated string, where the first token is the plugin name.
Events starting with "phantom" are reserved for use by the Phantom core.

### Actions
Plugins may register actions they provide.  Like events, actions consist of a
dot separated string, where the first token is the plugin name.  Actions
starting with "phantom" are reserved for use by the Phantom core.  When the
Phantom core executes an action it will call into the plugin, passing in any
arguments to the action specified in the phantomrc.

Actions may return a value indicating an error.  These return values may be
passed to and used by subsequent actions.


## Phantomrc
Phantomrc is a JSON format configuration file loaded by Phantom when it
starts.  By default, Phantom looks for a file called `.phantomrc` in the home
directory then the working directory.  This may be overridden with by passing a
file path as a command line argument to Phantom.
```javascript
{
	"plugins": ...,
	"options": ...,
	"events": ...,
	"macros:: ...
}
```

### Plugins
Array of plugin names to load.  Plugins are activated and deactivated in the
order listed in the array.
```javascript
"plugins": ["phsignal", "phi"]
```
When plugins are activated, a special "phantom.activate" event is produced for
each plugin.  Likewise, when each plugin is deactivated a "phantom.deactivate"
event is produced.

### Options
* `jobs` - Number of threads to ise for executing macros.
* `plugins` - Arguments to pass to plugins when they are activated.
* `verbose` - Show debug messages in console.
```javascript
"options": {
	"jobs": 2,
	"plugins": {
		"phsignal": {"signals": ["interrupt", "quit", "terminate"]}
	}
	"verbose": true
}
```

### Events
List of events to listen for and the macros to execute when each event is
received.
`*` may be used as a wildcard at the end of event names.

When an event occurs, Phantom enqueues the macros listed in the array for the
given event.  Macros are executed in an arbitrary order, and may run in parallel
up to the number of jobs specified in the options.
```javascript
"events": {
	"signal.interrupt": ["exit"],
	"irw.KEY_SLEEP": ["close_window"],
}
```
Events starting with "phantom" are reserved for use by the Phantom core.

A "phantom.start" event is produced after starting the core Phantom
functionality and before activating plugins.  Once all plugins have been
loaded, a "phantom.running" event is produced.  When Phantom exits, a
"phantom.stopping" event is produced before deactivating plugins.  Once all
plugins have been deactivated a "phantom.exit" event is produced.  Caution
should be taken when using these events to tigger macros, as some plugins may
not be active.

### Macros
Each macro consists of a list of actions to perform.  Actions are executed
sequentially in the order they appear.  The action to perform is specified by
"action", and any arguments to pass to the action are listed under "config".
```javascript
"macros": {
	"exit": [{"action": "phantom.exit"}],
	"close_window": [{"action": "xdotool.keyboard.key", "config": {"key": "alt+F4"}}],
}
```
When a macro is started, a special "phantom.macro" event is produced.  Each
action in the macro likewise produces a "phantom.action" event when it is
executed.  Caution should be taken when using these events to trigger macros,
as they can easily lead to endless loops.

Actions starting with "phantom" are reserved for use by the Phantom core.

#### Variables
Variables may be used in action configurations.
* `$event` - Event name.
* `$macro` - Macro name.
* `$action` - Action name.
* `$prev` - Exit code from last action in the macro.

### Minimum Configuration
At a minimum, the phantomrc should consist of the following.  This configuration
allows the user to exit Phantom by pressing `ctrl+c`, without this there
is no way to exit Phantom gracefully.
```javascript
{
	"plugins": ["phsignal"],
	"events": {
		"signal.interrupt": ["exit"]
	}
	"macros": {
		"exit": [{"action": "phantom.exit"}]
	}
}
```


## Phsignal
Phsignal is a signal handling plugin provided for Phantom by default.  It is
also the only plugin required by the default configuration.  Phsignal listens
for POSIX signals sent to the Phantom process.  This allows Phantom to exit
gracefully if the `interrupt`, `quit`, or `terminate` signals are received (i.e.
from `ctrl+c`).


## Phi
Phi is essentially the Phantom standard library.  It provides a variety of
useful actions to make working with Phantom's macros more powerful.

### Variables
Phi provides a mechanism to store variables that are accesible across macros.

New variables can be added, or existing ones changed, using the "phi.set"
action.
```javascript
{"action": "phi.set", "config": {"key": "my_var", "value": "foo"}}
```
The value of a variable can be retrieved using the "phi.get" action and the
`$prev` variable.
```javascript
{"action": "phi.get", "config": {"key": "my_var"}},
{"action": "osd.display.text", "config": {"text": "$prev"}},
```
Variabled may be removed with the "phi.unset" action.
```javascript
{"action": "phi.unset", "config": {"key": "my_var"}}
```

### Events
Phi provides a couple way to produce new events from within a macro.  This first
and most simple is the "phi.event" action.
```javascript
{"action": "phi.event", "config": {"event": "my.event_name"}}
```
Events can be produced conditionally with the "phi.if" action.  If the value of
the "if" argument evaluates to True or a non-zero value, the event specified
by "true" is produced, otherwise the event specified by "false" is produced.
Either or both of these may be absent, in which case no event will be produced
for that case.
```
{"action": "phi.if", "config": {"if": "$prev",
	"true": "my.if_event",
	"false": "my.else_event"
}}
```
Events can also be produced with the "phi.iswitch" action.  If the value of
the "switch" argument evaluates equal to one of the keys specified in the
"case" argument, the corresponding event is produced.  If none of the cases
match the event specified, if any, by the "default" argument is produced.
Either or both of these may be absent, in which case no event will be produced
for that case.
```
{"action": "phi.switch", "config": {"switch": "$prev",
	"case": {
		"0": "my.event_zero",
		"1": "my.event_one",
		"abc": "my.event_a",
		"def": "my.event_d"
	},
	"default": "my.event_no_match"
}}
```

### Operators
Phi provides basic comparison and arithmetic operators.  The arguments "left"
and "right" specify the values for each operator.  The result can be retrieved
using the `$prev` variable.
* `phi.equal` - Equality operator.
* `phi.notequal` - Inequality operator.
* `phi.add` - Addition operator.
* `phi.subtract` - Subtraction operator.
* `phi.multiply` - Multiplication operator.
* `phi.divide` - Division operator.
* `phi.modulo` - Modulo operator.

```
{"action": "phi.get", "config": {"key": "my_var"}}
{"action": "phi.add", "config": {"left": "$prev", "value": 1}}
{"action": "phi.set", "config": {"key": "my_var", "value": "$prev"}}
```
