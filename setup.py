from setuptools import setup, find_packages

setup(
	name='Phantom',
	version='1.0',
	packages=find_packages(),
	url='https://gitlab.com/cwesson/Phantom',
	license='GPLv3',
	author='Conlan Wesson',
	author_email='conlan.wesson@gmail.com',
	description='Event driven task runner for Linux',
	entry_points={
		'console_scripts': [
			'phantom = phantom.core.phantom:main'
		]
	}
)

